require('../style/index.styl')
const entries = require('./entries.js')

class Controller {
  constructor() {
    this.maxPoints = 50

    this.pointsRemaining = parseFloat(window.localStorage.pointsRemaining)

    if (this.pointsRemaining == undefined || Number.isNaN(this.pointsRemaining))
    {
      this.pointsRemaining = this.maxPoints
    }

    this.displayedPoints = this.pointsRemaining
  }

  addPoints(points) {
    this.pointsRemaining += parseFloat(points)
    this.pointsRemaining = Math.min(this.pointsRemaining, this.maxPoints)
    window.localStorage.pointsRemaining = this.pointsRemaining
    this.startCountdown()
  }

  async init() {
    $('.points-remaining').text(this.pointsRemaining)
    $('#food-buttons > a.entry').click((e) => {
      let idx = $(e.currentTarget).data('idx')
      this.countFood(entries[idx])
    })

    $('#refresh-button').click((e) => {
      this.pointsRemaining = this.maxPoints
      this.addPoints(0)
      this.addHistory(-this.maxPoints, "↺", "Refresh")
    })

    $('#flare-up').click((e) => {
      this.pointsRemaining = -25
      this.addPoints(0)
      this.addHistory(25, "😡", "Flare Up")
    })

    $('#burning').click((e) => {
      this.pointsRemaining /= 2
      this.addPoints(0)
      this.addHistory(this.pointsRemaining, "😰", "Burning")
    })

    window.setInterval(() => this.refreshPoints(), 3000)
  }

  async addHistory(points, icon, name)
  {
    let firstChild = $('#history-list > li:first-child()')
    if (firstChild.attr("name") == name)
    {
      let oldPoints = + firstChild.attr("points")
      oldPoints += points
      firstChild.attr("points", oldPoints)
      firstChild.find('.entry-points').text(`${oldPoints < 0 ? "+" : "-"}${Math.abs(Math.round(oldPoints))}`)
      return
    }

    var template = $('#history-list > .template').clone()
    template.removeClass('hidden').removeClass('template')
    template.find('.entry-points').text(`${points < 0 ? "+" : "-"}${Math.abs(Math.round(points))}`)
    template.find('.entry-icon').text(icon)
    template.find('.entry-name').text(name)

    for (let [k,v] of Object.entries({points, icon, name}))
    {
      template.attr(k,v)
      console.log(k, template.attr(k))
    }

    $('#history-list').prepend(template)
    console.log("Adding", template, template.attr('name'))
  }

  async countFood(entry) {
    this.addPoints(-entry.points)
    this.addHistory(entry.points, entry.icon, entry.name)
  }

  async refreshPoints() {
    if (!this.lastRefresh) this.lastRefresh = performance.now()

    let accumTime = (performance.now() - this.lastRefresh) / 1000;

    const secondsInADay = 86400
    const secondsForPoint = secondsInADay / 1.5

    if (accumTime > secondsForPoint)
    {
      var points = accumTime / secondsForPoint
      this.lastRefresh = performance.now()
      this.addPoints(points)
      this.addHistory(-points, "🙂", "Relax")
    }
  }

  startCountdown() {
    window.requestAnimationFrame(this.animateCountdown.bind(this))
  }

  async animateCountdown(timestamp) {
    if (!this.startAnimateCountdown) this.startAnimateCountdown = timestamp;
    let progress = (timestamp - this.startAnimateCountdown) / 1000;
    var done = false;
    if (this.displayedPoints > this.pointsRemaining)
    {
      this.displayedPoints -= progress

      if (this.displayedPoints <= this.pointsRemaining)
      {
        done = true
      }
    }
    else
    {
      this.displayedPoints += progress

      if (this.displayedPoints >= this.pointsRemaining)
      {
        done = true
      }
    }

    if (done)
    {
      this.displayedPoints = this.pointsRemaining
      this.startAnimateCountdown = undefined
    }

    $('.points-remaining').text(Math.round(this.displayedPoints))

    if (!done)
    {
      this.startCountdown()
    }
  }
}

window.Controller = new Controller()

$(document).ready(() => {
  window.Controller.init()
})
