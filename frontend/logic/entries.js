module.exports = [
  {name: "Coffee", icon: "☕👩", points: 15},
  {name: "Ketchup", icon: "🍅🍟", image: "ketchup", points: 12},
  {name: "Drink", icon: "🍸", points: 20},
  {name: "Pizza", icon: "🍕", points: 8},
  {name: "Lemons", icon: "🍋", points: 10},
  {name: "Spicy Food", icon: "🍲", points: 30},
  {name: "Keifer", icon: "🥛", points: 10},
  {name: "Soy Sauce", icon: "🍶", points: 4},
  {name: "Tomato Soup", icon: "🍅🥣", image: "tomatosoup", points: 50},
]
