# Setup

1. Install nodejs: https://nodejs.org/en/
2. On the command line, from this directory, install dependencies: `npm install`
3. On the command line, from this directory, start the server: `node index.js`
4. Open a browser and go to `http://localhost:3000/`
