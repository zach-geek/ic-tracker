const path = require('path')

require.extensions['.jpg'] = () => {};
require.extensions['.jpeg'] = () => {};
require.extensions['.gif'] = () => {};
require.extensions['.png'] = () => {};
require.extensions['.svg'] = () => {};

module.exports = [
  {
    context: path.resolve(__dirname, 'frontend/'),
    entry: [
      './logic/index.js',
    ],
    mode: 'development',
    output: {
      publicPath: '/js',
      path: path.resolve(__dirname, 'static/js'),
      filename: 'index.js'
    },
    module: {
      rules: [
        {
          test:/\.(jpe?g|png|gif|svg)$/i,
          loader: 'file-loader?name=/img/[name].[ext]'
        },
        {
          test: /\.css$|\.styl$/,
          loader: 'style-loader!css-loader!stylus-loader'
        },
      ]
    }
  }
]
