const Koa = require('koa')
const path = require('path')
const process = require('process')
const webpack = require('webpack');
const koaWebPack = require('koa-webpack')
const webpackConfig = require('./webpack.config.js')
const send = require('koa-send')

var slm = require('slm')
var markdown = require('slm-markdown')
markdown.register(slm.template)

const app = new Koa();

(async () => {
  for (var config of webpackConfig)
  {
    if (process.env.PORT)
    {
      app.use(await koaWebPack({compiler: webpack(config), hotClient: false}))
    }
    else
    {
      app.use(await koaWebPack({compiler: webpack(config)}))
    }
  }
})();


app.ROOT_DIR = path.resolve(__dirname)
var views = require('koa-views');
app.use(views(app.ROOT_DIR + '/frontend', {
  options: {
    useCache: false
  }
}))

const Router = require('koa-router');
var router = new Router();

router.get('/', async (ctx, next) => {
  await ctx.render('index.slm', {session: ctx.session})
})

router.get('/images/:file.png', async (ctx, next) => {
  await send(ctx, `./frontend/images/${ctx.params.file}.png`)
})

app
  .use(router.routes())
  .use(router.allowedMethods())

app.listen(process.env.PORT || 3000)
